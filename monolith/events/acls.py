from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY
import requests
import json


def get_photo(city, state):
    # Use the Pexels API
    headers = {"Authorization": PEXELS_API_KEY}
    params ={
        "per_page": 1,
        "query": city + " " + state
    }
    url = "https://api.pexels.com/v1/search"
    response = requests.get(url, params=params, headers=headers)
    content = json.loads(response.content)
    try:
        return {"picture_url": content["photos"][0]["src"]["original"]}
    except (KeyError, IndexError):
        return {"picture_url": None}



# def get_weather_data(city, state):
#     # Use the Open Weather API
#     params = {
#         "q": f"{city},{state},US",
#         "limit": 1,
#         "appid": OPEN_WEATHER_API_KEY,
#     }
#     url = "http://api.openweathermap.org/geo/1.0/direct"
#     response = requests.get(url, params=params)
#     content = json.loads(response.content)
#     try:
#         latitude = content[0]["lat"]
#         longitude = content[0]["lon"]
#     except (KeyError, IndexError):
#         return None

#     params = {
#         "lat": latitude,
#         "lon": longitude,
#         "appid": OPEN_WEATHER_API_KEY,
#         "units": "imperial",
#     }
#     url = "https://api.openweathermap.org/data/2.5/weather"
#     response = requests.get(url, params=params)
#     content = json.loads(response.content)

#     try:
#         return {
#             "desciption": content["weather"][0]["description"],
#             "temp": content["main"]["temp"],
#         }
#     except (KeyError, IndexError):
#         return None
        
#     return weather
def get_coordinates(city, state):
    url = f"http://api.openweathermap.org/geo/1.0/direct?q={city},{state},US$$limit=5&appid={OPEN_WEATHER_API_KEY}"
    response = requests.get(url)
    lat = response.json()[0]["lat"]
    lon = response.json()[0]["lon"]
    return {"lat": lat, "lon": lon}


def get_weather_data(lat, lon):
    # Use the Open Weather API
    url = f"https://api.openweathermap.org/data/2.5/weather?lat={lat}&lon={lon}&units=imperial&appid={OPEN_WEATHER_API_KEY}"
    response = requests.get(url)
    return {
        "description": response.json()["weather"][0]["description"],
        "temp": response.json()["main"]["temp"],
    }


    